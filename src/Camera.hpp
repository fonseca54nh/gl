#pragma once

#ifndef __CAMERA_HPP
#define __CAMERA_HPP

#include "header.hpp"

class Camera
{
	public:
		//glm::vec3 Position = glm::vec3(0,0,0), Orientation = glm::vec3(0,0,0), Up=glm::vec3(0,0,0);
		//glm::mat4 view    , projection , model;
		//float width       , height     , speed, sensitivity;
		GLFWwindow* window;
		GLuint shader;
		double xpos, ypos;

	public:
		Camera( GLFWwindow* w, GLuint s ) : window( w ), shader( s ){ init(); };//init(); updateView(); };
		//Camera( GLFWwindow* w, glm::vec3 p, glm::vec3 o, glm::vec3 u ) : window( w ), Position( p ), Orientation( o ), Up( u ) { updateView(); };

		void init();
		void updateView();
		void input();
		void updateShader( GLuint );
			
};
#endif
