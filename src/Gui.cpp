#include "Gui.hpp"

Gui::Gui()
{
	GLfloat uiVertices[] = {
			-1.f, -1.f, 0,
			+1.f, -1.f, 0,
			+1.f, +1.f, 0,
			+1.f, +1.f, 0,
			-1.f, +1.f, 0,
			-1.f, -1.f, 0,
	};

	GLfloat uiTexCoords[] = {
		+1.f, +1.f,
		+1.f, -1.f,
		-1.f, +1.f,
		-1.f, -1.f,
	};

	glGenVertexArrays( 1, &vao );
	glGenBuffers( 1, &vbo );

	glBindVertexArray( vao );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	
	glBufferData( GL_ARRAY_BUFFER, sizeof( uiVertices ), uiVertices, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

Gui::Gui( GLfloat *uiVertices ) : vertices( uiVertices )
{
	glGenVertexArrays( 1, &vao );
	glGenBuffers( 1, &vbo );

	glBindVertexArray( vao );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	
	glBufferData( GL_ARRAY_BUFFER, sizeof( uiVertices ), uiVertices, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

Gui::Gui( GLfloat *uiVertices, GLint s ) : vertices( uiVertices ), shader( s )
{
	glGenVertexArrays( 1, &vao );
	glGenBuffers( 1, &vbo );

	glBindVertexArray( vao );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	
	glBufferData( GL_ARRAY_BUFFER, sizeof( uiVertices ), uiVertices, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

void Gui::draw()
{
			glEnable( GL_STENCIL_TEST );
			glClear(GL_DEPTH_BUFFER_BIT);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			//glOrtho(640/480.f, 640, 0, 480, 0, 1);
			glOrtho(width/height, width, 0, height, 0, 1);
			glLoadIdentity();

			glUseProgram(shader);
			glBindVertexArray(vao);
			glDrawArrays(GL_TRIANGLES, 0, 18);
}

void Gui::useShader( GLint s ) { shader = s; }

void Gui::print()
{
	for( auto i = 0; i < sizeof( vertices ); ++i )
		std::cout << vertices[i] << std::endl;
}
