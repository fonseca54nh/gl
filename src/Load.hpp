#pragma once

#ifndef __LOAD_HPP
#define __LOAD_HPP

#include "header.hpp"
//#include "Shader.hpp"
#include "Texture.hpp"

//inline Shader shader( "shaders/v.glsl", "shaders/f.glsl" );
//inline GLuint shaderProgram = shader.getProgram();
//inline Texture imageTexture;

class Load
{
	protected:
		mesh tempModel;
		unsigned int vao, vbo, texVbo, texture, depthVbo, depthMap;
		GLuint FramebufferName = 0;
		const unsigned int sw = 1024, sh = 1024;
		std::string filePath, texturePath;
		//void* vertices;
		GLuint shader;
		int size = 0;
		//glm::mat4 view = glm::mat4(1.0f), projection=glm::mat4(1.0f), model=glm::mat4(1.0f);
		Texture imageTexture;
		
	public:
		//Load( std::string a, std::string b );
		// This is interesting thinking of an engine
		// then you could declare an object without a initialized shader
		// but this would imply in it not having a texture
		// hence you would need to declare more functions to add a texture to this object
		// and modify this constructor
		//Load( GLfloat & );
		//Load( std::string a );
		Load( std::string a, std::string b, GLuint s );
		mesh readfile();

		void draw();
		void draw( float, float, float, int );
		void draw( float, float, float, int, float );

		void useShader( GLuint );

		glm::mat4 getModel();

		void bind( std::string );
};

#endif
