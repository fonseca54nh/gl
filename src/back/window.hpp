#pragma once

#include "header.hpp"

class Window
{
	protected:
		GLFWwindow* mWindow;
		int mWidth, mHeight, mError;	
		std::string mName;
		static const char* mD;
	public:
		Window();
		Window( int width, int height, std::string name ) : mWidth( width ), mHeight( height ), mName( name ) {};

		GLFWwindow* init();
		static void error();
		void sizeCallback();
		int getWidth();
		int getHeight();
};
