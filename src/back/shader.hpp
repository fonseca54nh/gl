#pragma once

#include "camera.hpp"

class Shader : public Camera
{
	protected:
		std::string mVertexShaderPath   , mFragShaderPath;
		std::string mVertexShaderSource , mFragShaderSource;

		unsigned int mVertexShader, mFragShader, mShaderProgram;

		int mCompilationSuccess;
		char mInfoLog[512];

		std::string basicVShader = "#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"uniform mat4 camMatrix;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = camMatrix * vec4(aPos, 1.0);\n"
		"}\0";

		std::string basicFShader = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(0.7f,0.3f,0.2f,1.0f);\n"
		"}\0";

	public:
		Shader() { };//copy(basicVShader.begin(), basicVShader.end(),mVertexShaderSource.begin()); };
		Shader( std::string vertexShaderPath, std::string fragShaderPath ) :
						mVertexShaderPath(vertexShaderPath),
						mFragShaderPath(fragShaderPath)
		{ 
			mVertexShaderSource = glCreateShader( GL_VERTEX_SHADER );
			mFragShaderSource   = glCreateShader( GL_FRAGMENT_SHADER );

			mVertexShaderSource = basicVShader;
			mFragShaderSource   = basicFShader;
		};

		//@brief get shader contents @param std::string shader aka shader to be read
		std::string getShader( std::string shader ); 

		void compile();

		// create a shader "parser" aka simplify existing glsl, aka "custom" shader lang
};

