#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <thread>
#include <mutex>
#include <map>
#include <vector>
#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>

struct face
{
	std::vector<int> indices;
	std::vector<int> texIndex;
	std::vector<int> normalIndex;
};

struct vertice
{
	GLfloat point[3];
};

struct obj
{
	std::vector<vertice> vertices;
	std::vector<float> texCoord;
	std::vector<float> normals;
	face faces;
};

obj parseFile()
{
	std::ifstream file( "objs/b.obj", std::fstream::in  );
	std::string sourceCode;
	obj og;
	vertice temp;

	while( getline(file, sourceCode) )
	{
		// take the vertices from the string
		if( !sourceCode.find("v ") )
		{
			// erase the 'v' and the ' ' after it
			sourceCode.erase(0, 2);

			//temp string
			std::string token;
			auto i = 0;
			while( i != 3 )
			{
				// take the number until the first space
				token = sourceCode.substr(0, sourceCode.find_first_of(" "));
				// erase that number from the string
				sourceCode.erase( 0, sourceCode.find_first_of( " " )+1 );

				// cast to a float
				//char *e;
				std::string::size_type e;
				GLfloat v = std::stof( token, &e );
				// push_back float to vertices
				//og.vertices.push_back( v );
				if( i == 0 ) temp.point[0] = v;
				if( i == 1 ) temp.point[1] = v;
				if( i == 2 ) temp.point[2] = v;
				++i;
			}

				og.vertices.push_back( temp );
		}

		// take the texture coordinates from the string
		if( !sourceCode.find("vt ") )
		{
			// erase the 'vt' and the ' ' after it
			sourceCode.erase(0, 3);

			//temp string
			std::string token;
			auto i = 0;
			while( i != 2 )
			{
				// take the number until the first space
				token = sourceCode.substr(0, sourceCode.find_first_of(" "));
				// erase that number from the string
				sourceCode.erase( 0, sourceCode.find_first_of( " " )+1 );

				// cast to a float
				char *e;
				float v = strtof( token.c_str(), &e );
				// push_back float to vertices
				og.texCoord.push_back( v );
				++i;
			}
		}
		
		// take the normals from the string
		if( !sourceCode.find("vn ") )
		{
			// erase the 'vt' and the ' ' after it
			sourceCode.erase(0, 3);

			//temp string
			std::string token;
			auto i = 0;
			while( i != 3 )
			{
				// take the number until the first space
				token = sourceCode.substr(0, sourceCode.find_first_of(" "));
				// erase that number from the string
				sourceCode.erase( 0, sourceCode.find_first_of( " " )+1 );

				// cast to a float
				char *e;
				float v = strtof( token.c_str(), &e );
				// push_back float to vertices
				og.normals.push_back( v );
				++i;
			}
		}

		// take the faces indices from the string
		if( !sourceCode.find("f ") )
		{
			// erase the 'v' and the ' ' after it
			sourceCode.erase(0, 2);

			//temp string and vector
			std::string token;
			std::vector<std::string> temp;

			// split the line into 'words'
			auto i = 0;
			while( i != 3 )
			{
				token = sourceCode.substr( 0, sourceCode.find_first_of( " " ) );
				temp.push_back( token );
				sourceCode.erase( 0, sourceCode.find_first_of( " " )+1 );
				++i;
			}

			// foreach word tokenize aka get indice from 'word'
			for( auto x : temp )
			{
				auto j = 0;
				while( j != 3 )
				{
					token = x.substr( 0, x.find_first_of( "/" ) );
					if( j == 0 ) og.faces.indices.push_back( stoi( token ) );
					if( j == 1 ) og.faces.texIndex.push_back( stoi( token ) );
					if( j == 2 ) og.faces.normalIndex.push_back( stoi( token ) );
					x.erase( 0, x.find_first_of( "/" ) + 1 );
					++j;
				}
			}
		}
	}

	//for( auto x : og.vertices )
	//{
	//	std::cout << x << std::endl;
	//}

	return og;
}

void error( int error, const char* d ) { std::cout << "error: " << d << std::endl; }
void sizeCallback( GLFWwindow* window, int width, int height ) { glViewport( 0, 0, width, height ); }

auto getShader( std::string shader ) -> std::string
{
	std::ifstream frag( shader, std::fstream::in );
	std::string sourceCode;

	if( frag.is_open() ) sourceCode = std::string( std::istreambuf_iterator<char>(frag),
																								 std::istreambuf_iterator<char>() );
	return sourceCode;
};

void compileShader( unsigned int shader, std::string type )
{
	glCompileShader( shader );

	int compilationSuccess;
	char infoLog[512];
	glGetShaderiv( shader, GL_COMPILE_STATUS, &compilationSuccess );

	if( !compilationSuccess )
	{
		glGetShaderInfoLog( shader, 512, NULL, infoLog );
		std::cout << type << "ShaderCompilationError: " << infoLog << std::endl;
	}
	else
	{
		std::cout << type << "ShaderCompilationSucessfull" << std::endl;
	}
}

unsigned int dynaRec( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( "v.glsl" );
	auto ftemp = getShader( "f.glsl" );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

int main()
{
	if( !glfwInit() ) { std::cout << "init error" << std::endl; }

	GLFWwindow* window = glfwCreateWindow( 640, 480, "Bread", NULL, NULL );

	if( !window ) std::cout << "window error" << std::endl;

	glfwSetErrorCallback           ( error                ) ;
	//glfwSetKeyCallback             ( window, keys         ) ;
	glfwMakeContextCurrent         ( window               ) ;
	glfwSetFramebufferSizeCallback ( window, sizeCallback ) ;
	glewInit();

	glEnable( GL_LIGHTING );
	glEnable( GL_DEPTH_TEST );
  //glShadeModel(GL_FLAT);
  //glDisable(GL_CULL_FACE);

	auto vertexShader   = glCreateShader( GL_VERTEX_SHADER );
	auto fragmentShader = glCreateShader( GL_FRAGMENT_SHADER );

	auto shaderProgram = glCreateProgram();
	shaderProgram = dynaRec( vertexShader, fragmentShader, shaderProgram );
	glUseProgram( shaderProgram );

	// crete a set of vertices
	//float vertices[] = {
  //  -0.5f, -0.5f, 0.0f,
  //   0.5f, -0.5f, 0.0f,
  //   0.0f,  0.5f, 0.0f
	//};

	obj vv = parseFile();
	std::vector<vertice> v = vv.vertices;
	std::vector<int> i     = vv.faces.indices;

	//std::reverse( v.begin(), v.end() );
	//std::reverse( i.begin(), i.end() );
	std::reverse( vv.faces.indices.begin(), vv.faces.indices.end() );
	GLfloat vertices[] =
	{
		-0.5f, -0.5f     * float(sqrt(3)) / 3, 0.0f, // Lower left corner
		 0.5f, -0.5f     * float(sqrt(3)) / 3, 0.0f, // Lower right corner
		 0.0f, 0.5f      * float(sqrt(3)) * 2  / 3, 0.0f, // Upper corner
		-0.5f / 2, 0.5f  * float(sqrt(3)) / 6, 0.0f, // Inner left
		 0.5f / 2, 0.5f  * float(sqrt(3)) / 6, 0.0f, // Inner right
		 0.0f, -0.5f     * float(sqrt(3)) / 3, 0.0f // Inner down
	};

	GLfloat cv[] =
	{
				-1, -1,  0.5, //0
         1, -1,  0.5, //1
        -1,  1,  0.5, //2
         1,  1,  0.5, //3
        -1, -1, -0.5, //4
         1, -1, -0.5, //5
        -1,  1, -0.5, //6
         1,  1, -0.5  //7
	};

static const GLfloat stupidCube[] = {
		-1.0f,-1.0f,-1.0f, //triangle 1 : begin
		-1.0f,-1.0f,1.0f,
		-1.0f,1.0f,1.0f,  //triangle 1 : end
		1.0f, 1.0f,-1.0f, // triangle 2 : begin
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f, // triangle 2 : end
		1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		1.0f,-1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f,-1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f,-1.0f, 1.0f
	};

	GLfloat ci[] =
	{
				//Top
				2, 6, 7,
        2, 3, 7,

        //Bottom
        0, 4, 5,
        0, 1, 5,

        //Left
        0, 2, 6,
        0, 4, 6,

        //Right
        1, 3, 7,
        1, 5, 7,

        //Front
        0, 2, 3,
        0, 1, 3,

        //Back
        4, 6, 7,
        4, 5, 7
	};
	//int xsize = 20, zsize = 20;

	//glm::vec3 vertices[(xsize+1)*(zsize+1)] = {};
	//auto i = 0;
	//for( auto y = 0; y < xsize; y++ )
	//	for( auto x = 0; x < zsize; x++ )
	//	{
	//		vertices[i] = glm::vec3(x, y, 0);
	//		i++;
	//		std::cout << "(" << x << "," << y << "," << 0 << ")" << std::endl;
	//	}

	//int indices[xsize*zsize*6];
	//auto vert = 0, tris = 0;
	//for ( auto x = 0; x < xsize; x++ )
	//{
	//	indices[tris + 0] = vert + 0;
	//	indices[tris + 1] = vert + xsize + 1;
	//	indices[tris + 2] = vert + 1;
	//	indices[tris + 3] = vert + 1;
	//	indices[tris + 4] = vert + xsize + 1;
	//	indices[tris + 5] = vert + xsize + 2;
	//	vert++;
	//	tris+=6;
	//	std::cout << "==========" << std::endl;
	//	std::cout <<  vert + 0                    << std::endl;
	//	std::cout <<  vert + 1                    << std::endl;
	//	std::cout <<  vert + xsize + 1            << std::endl;
	//	std::cout <<  vert + 1                    << std::endl;
	//	std::cout <<  vert + xsize + 2            << std::endl;
	//	std::cout <<  vert + xsize + 1            << std::endl;
	//	std::cout << "==========" << std::endl;
	//}
			
	//unsigned int indices[] = { 2,1,0 };
	int indices[] =
	{
		0, 3, 5,
		3, 2, 4,
		5, 4, 1
	};


	GLfloat ver[] = {

	1.000000,1.000000,-1.000000,
 1.000000,-1.000000,-1.000000,
 1.000000,1.000000,1.000000,
 1.000000,-1.000000,1.000000,
 -1.000000,1.000000,-1.000000,
 -1.000000,-1.000000,-1.000000,
 -1.000000,1.000000,1.000000,
 -1.000000,-1.000000,1.000000
	};

	int index[] = 
	{
		 3,7,5,1,
		 8,7,3,4,
		 6,5,7,8,
		 8,4,2,6,
		 4,3,1,2,
		 2,5,1,2
	};

	const int size = vv.vertices.size();
	GLfloat pp[size];
	for( auto i = 0; i < vv.vertices.size(); ++i )
	{
		for( auto j = 0; j < 3; ++j )
			pp[i] = v[i].point[j];
	}
	for( auto x : pp ) std::cout << x << " ";
	std::cout << "Hello There" << std::endl;
	std::cout << sizeof(stupidCube) << std::endl;
	std::cout << sizeof(vv.vertices) << std::endl;

	
	//for( auto i = 0; i < vv.vertices.size(); ++i )
	//{
	//	for( auto j = 0; j < 3; ++j )
	//		std::cout << v[i].point[j] << " ";
	//	std::cout << std::endl;
	//}
	//for( auto x : i ) std::cout << x << std::endl;
	unsigned int VBO, VAO, EBO;

	glGenVertexArrays( 1, &VAO );
	glGenBuffers( 1, &VBO );
	glGenBuffers(1, &EBO);

	glBindVertexArray( VAO );

	// literally points(bind) the ARRAY_BUFFER to the VBO
	glBindBuffer( GL_ARRAY_BUFFER, VBO );
	// literally sends s* to the buffer
//	glBufferData( GL_ARRAY_BUFFER, v.size()*sizeof(float)*9, &v[0], GL_STATIC_DRAW );
	
	//glBufferData( GL_ARRAY_BUFFER, v.size()*sizeof(float), &v[0], GL_STATIC_DRAW );
	glBufferData( GL_ARRAY_BUFFER, 36*4, &vv.vertices[0], GL_STATIC_DRAW );

	// literally points(bind) the ELEMENT_ARRAY_BUFFER to the VAO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	// literally sends s* to the buffer
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, vv.faces.indices.size()*sizeof(unsigned int), &vv.faces.indices[0], GL_STATIC_DRAW);

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//unsigned int VBO1, VAO1, EBO1;

	//glGenVertexArrays( 1, &VAO1 );
	//glGenBuffers( 1, &VBO1 );
	//glGenBuffers(1, &EBO1);

	//glBindVertexArray( VAO1 );

	//// literally points(bind) the ARRAY_BUFFER to the VBO
	//glBindBuffer( GL_ARRAY_BUFFER, VBO1 );
	//// literally sends s* to the buffer
	//glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW );

	//// literally points(bind) the ELEMENT_ARRAY_BUFFER to the VAO
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO1);
	//// literally sends s* to the buffer
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3*sizeof( float ), ( void* )0 );
	//glEnableVertexAttribArray( 0 );

	//glBindBuffer( GL_ARRAY_BUFFER, 0 );
	//glBindVertexArray( 0 );
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////Camera ///////////////////////////////////////////////
	glm::vec3 Position = glm::vec3(0.0f, 0.0f, 2.0f);
	glm::vec3 Orientation = glm::vec3( 0.0f, 0.0f, -1.0f );
	glm::vec3 up = glm::vec3( 0.0f, 1.0f, 0.0f );
	float width = 640, height = 480;
	float speed = 0.1f, sensitivity = 100.0f;

	glm::mat4 view       = glm::mat4(1.0f);
	glm::mat4 projection = glm::mat4(1.0f);
	glm::mat4 model		   = glm::mat4(1.0f);

	view = glm::lookAt( Position, Position + Orientation, up );
	projection = glm::perspective( glm::radians(45.0f), (float)(width / height), 0.1f, 100.f );

	glUniformMatrix4fv( glGetUniformLocation( shaderProgram, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );

	//matrix( 45.0f, 0.1f, 100.0f, shaderProgram, "camMatrix" );
////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////Camera ///////////////////////////////////////////////

			//parseFile();
	bool lineLoop = false;
	while( !glfwWindowShouldClose( window ) )
	{

			glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
			//glClear( GL_COLOR_BUFFER_BIT );
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
			//shaderProgram = dynaRec( vertexShader, fragmentShader, shaderProgram );
			//glUseProgram( shaderProgram );

			if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			{
				Position += speed * Orientation;
			}
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			{
				Position += speed * -glm::normalize(glm::cross(Orientation, up));
			}
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			{
				Position += speed * -Orientation;
			}
			if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			{
				Position += speed * glm::normalize(glm::cross(Orientation, up));
			}
			if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
			{
				Position += speed * up;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
			{
				Position += speed * -up;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			{
				speed = 0.4f;
			}
			else if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
			{
				speed = 0.1f;
			}
			if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS )
			{
				lineLoop = !lineLoop;	
			}
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
			{
				glfwSetWindowShouldClose(window, true);
			}
			if ( glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS )
			{
				glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN );

				double mouseX, mouseY;
				glfwGetCursorPos( window, &mouseX, &mouseY );
				float rotX = sensitivity * (float)( mouseX - (height / 2) )/height;
				float rotY = sensitivity * (float)( mouseY - (height / 2) )/height;

				glm::vec3 newOrientation = glm::rotate( Orientation, glm::radians(-rotX), glm::normalize(glm::cross(Orientation, up)) );
				if( !((glm::angle(newOrientation, up) <= glm::radians(5.0f) or (glm::angle(newOrientation, -up) <= glm::radians(5.0)))))
				{
					Orientation = newOrientation;
				}
				Orientation = glm::rotate(Orientation, glm::radians(-rotY), up);
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS )
			{
				Position -= speed * Orientation.x + .1;
			}

			view = glm::lookAt( Position, Position + Orientation, up );
			//view = glm::lookAt( glm::vec3(4.0f, 3.f, -3.f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,1.0f,0.0f) );
			projection = glm::perspective( glm::radians(45.0f), (float)(width / height), 0.1f, 100.f );
			glUniformMatrix4fv( glGetUniformLocation( shaderProgram, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );
			
			//float time = glfwGetTime();
			//GLint i = glGetUniformLocation( shaderProgram, "xoffset" );
			//glUniform1f(i, time);

			//glBindVertexArray( VAO );
			//glDrawArrays( GL_LINE_LOOP, 0, 3 );
			//if( lineLoop == true )
			//{
			//	glBindVertexArray( VAO );
			//	glDrawElements(GL_LINE_LOOP, 9, GL_UNSIGNED_INT, 0);
			//}
			//else
			//{
			//	glBindVertexArray( VAO );
			//	glDrawElements(GL_TRIANGLES, 9, GL_UNSIGNED_INT, 0);
			//}

			glBindVertexArray(VAO);
			//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			//glDrawArrays(GL_TRIANGLES, 0, 24*6);
			//glDrawArrays(GL_LINE_LOOP, 0, 24*3);
		  glDrawElements(GL_LINES, 36, GL_UNSIGNED_INT, 0) ;

			glfwSwapBuffers( window );
			glfwSwapInterval( 1 );
			glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader); 
	glfwTerminate();
}
