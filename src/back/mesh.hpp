#pragma once

#include "shader.hpp"

//template<typename T>
class Mesh
{
	protected:
		drawMode mode;
		Shader mShader;
		//T mVertices;
		//T mIndices;
		//T mTexture;
		//T mUv;
		GLfloat* mVertices;
		GLfloat* mIndices;
		GLfloat* mTexture;
		GLfloat* mUv;
	public:
		Mesh() {};
		//Mesh( glm::vec3 vertices, Shader shader ) : mVertices(vertices), mShader(shader) {};
		//Mesh( glm::vec3 vertices, glm::vec3 indices, Shader shader ) : mVertices(vertices), mIndices(indices), mShader(shader) {};

		Mesh( GLfloat *vertices, GLfloat *indices, Shader shader ) : mVertices(vertices), mIndices(indices), mShader(shader) {};
		Mesh( GLfloat *vertices, GLfloat *indices ) : mVertices(vertices), mIndices(indices) {};
		//Mesh( T vertices, T indices ) : mVertices(vertices), mIndices(indices) 
		//{
		//};
		//Mesh( T vertices, T indices, Shader shader ) : mVertices(vertices), mIndices(indices), mShader(shader)
		//{
		//};

		//Mesh( glm::vec3 vertices,
		//			glm::vec3 indices,
		//			glm::vec2 uv,
		//			glm::vec3 texture,
		//			Shader shader) :
		//			mVertices(vertices),
		//			mIndices(indices), 
		//			mUv(uv),
		//			mTexture(texture), 
		//			mShader(shader) {};

		//T addVertices( T vertices );
		//T addIndices ( T indices  );

		//void draw( drawMode, int ) { Buffers<T>::BindVAO(); glDrawElements( mode, sizeof(mIndices), GL_UNSIGNED_INT, 0); };
		//void vboData()
		//{
		//	glBindBuffer( GL_ARRAY_BUFFER, Buffers<T>::VBO );
		//	//glBufferData( GL_ARRAY_BUFFER, sizeof( mVertices ), mVertices, GL_STATIC_DRAW );
		//};
		//void eboData()
		//{
		//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Buffers<T>::EBO);
		//	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(mIndices), mIndices, GL_STATIC_DRAW);
		//};
};
