#include "buffers.hpp"

unsigned int Buffers::getVAO() { return VAO; }
unsigned int Buffers::getVBO() { return VBO; }
unsigned int Buffers::getEBO() { return EBO; }

void Buffers::BindVAO() { glBindVertexArray( VAO ); }

void Buffers::Unbind()
{
	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

//void Buffers::draw(drawMode mode, int size)
void Buffers::draw(int size)
{
	this->BindVAO();
  glDrawElements( GL_TRIANGLES, 9, GL_UNSIGNED_INT, 0);
}

void Buffers::vboData()
{
	glBindBuffer( GL_ARRAY_BUFFER, VBO );
	glBufferData( GL_ARRAY_BUFFER,  sizeof( mVertices ), &mVertices, GL_STATIC_DRAW );
}

void Buffers::eboData()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(mIndices), &mIndices, GL_STATIC_DRAW);
}
