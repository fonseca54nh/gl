#include "shader.hpp"

std::string Shader::getShader( std::string shader )
{
	std::ifstream frag( shader, std::fstream::in );
	std::string sourceCode;

	if( frag.is_open() ) sourceCode = std::string( std::istreambuf_iterator<char>(frag),
																								 std::istreambuf_iterator<char>() );
	return sourceCode;
};

void Shader::compile()
{
	if( this->getShader( mVertexShaderPath ) != mVertexShaderSource )
	{
		auto v = this->getShader( mVertexShaderPath );
		auto *vertexShaderSource   = v.c_str();

		glShaderSource( mVertexShader, 1, &vertexShaderSource, NULL);
		
		glCompileShader( mVertexShader );
		glGetShaderiv( mVertexShader, GL_COMPILE_STATUS, &mCompilationSuccess );
		if( !mCompilationSuccess )
		{
			glGetShaderInfoLog( mVertexShader, 512, NULL, mInfoLog );
			std::cout << "vertexShaderCompilationError: " << mInfoLog << std::endl;
		}
		else std::cout << "vertexShaderCompilationSucessfull" << std::endl;
	}

	if( this->getShader( mFragShaderPath ) != mFragShaderSource )
	{
		auto f = this->getShader( mFragShaderPath );
		auto *fragmentShaderSource = f.c_str();
		glCompileShader( mFragShader );
		glGetShaderiv( mFragShader, GL_COMPILE_STATUS, &mCompilationSuccess );
		if( !mCompilationSuccess )
		{
			glGetShaderInfoLog( mFragShader, 512, NULL, mInfoLog );
			std::cout << "fragmentShaderCompilationError: " << mInfoLog << std::endl;
		}
		else std::cout << "fragmentShaderCompilationSucessfull" << std::endl;
	}

	glDeleteProgram( mShaderProgram );
	mShaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( mShaderProgram, mVertexShader );
	glAttachShader( mShaderProgram, mFragShader );
	glLinkProgram ( mShaderProgram );

	glUseProgram( mShaderProgram );
	glUniformMatrix4fv( glGetUniformLocation( mShaderProgram, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view ) );
}
