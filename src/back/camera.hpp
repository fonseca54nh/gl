#pragma once

#include "header.hpp"

class Camera
{
	protected:
		glm::vec3 mUp          = glm::vec3( 0.0f , 1.0f ,  0.0f );
		glm::vec3 mPosition    = glm::vec3( 0.0f , 0.0f ,  2.0f );
		glm::vec3 mOrientation = glm::vec3( 0.0f , 0.0f , -1.0f );

		float mWidth       = 640,
					mHeight      = 480,
					mSpeed       = 0.1f,
					mSensitivity = 100.0f;

		glm::mat4 view       = glm::mat4(1.0f);
		glm::mat4 projection = glm::mat4(1.0f);

	public:
		Camera() 
		{
			view = glm::lookAt( mPosition, mPosition + mOrientation, mUp ); 
		  projection = glm::perspective( glm::radians(45.0f), (float)(mWidth / mHeight), 0.1f, 100.f );
		};

		Camera( glm::vec3 Position,
						glm::vec3 Orientation,
						glm::vec3 Up,
						float width,
						float height,
						float speed,
						float sensitivity ) :
						mPosition(Position), 
						mOrientation(Orientation),
						mUp(Up),
						mWidth(width),
						mHeight(height),
						mSpeed(speed),
						mSensitivity(sensitivity)
						{ 
							view = glm::lookAt( Position, Position + Orientation, Up ); 
							projection = glm::perspective( glm::radians(45.0f), (float)(mWidth / mHeight), 0.1f, 100.f );
						};
};
