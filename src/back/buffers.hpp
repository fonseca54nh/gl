#pragma once

#include "mesh.hpp"

class Buffers : public Mesh
{
	protected:
		unsigned int VBO, VAO, EBO;
	public:
			Buffers()
			{
					glGenVertexArrays( 1, &VAO );
					glGenBuffers( 1, &VBO );
					glGenBuffers(1, &EBO);
					glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3*sizeof( float ), ( void* )0 );
					glEnableVertexAttribArray( 0 );
			};

			unsigned int getVAO();
			unsigned int getVBO();
			unsigned int getEBO();
			void BindVAO();
			//virtual void vboData() = 0;
			//virtual void eboData() = 0;
			void vboData();
			void eboData();
			void Unbind();

			//virtual void draw( drawMode mode, int size ) = 0;
			//void draw( drawMode mode, int size );
			void draw( int size );
};


