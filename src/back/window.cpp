#include "window.hpp"

GLFWwindow* Window::init()
{
	if( !glfwInit() ) { std::cout << "init error" << std::endl; }

	mWindow = glfwCreateWindow( mWidth, mHeight, mName.c_str(), NULL, NULL );

	if( !mWindow ) std::cout << "window error" << std::endl;

	//glfwSetErrorCallback          ( this->error          );
	//glfwSetKeyCallback            ( mWwindow, keys         );
	//glfwMakeContextCurrent        ( mWindow               );
	//glfwSetFramebufferSizeCallback( mWindow, sizeCallback );
	//glewInit();

	return mWindow;
}

int Window::getWidth()  { return mWidth; }
int Window::getHeight() { return mHeight; }

//void error() { std::cout << "mError: " << a.mD << std::endl; }
//void sizeCallback(){ glViewport( 0, 0, a.getWidth(), a.getHeight() ); }
