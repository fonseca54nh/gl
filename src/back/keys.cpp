
void keys()
{
			if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			{
				Position += speed * Orientation;
			}
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			{
				Position += speed * -glm::normalize(glm::cross(Orientation, up));
			}
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			{
				Position += speed * -Orientation;
			}
			if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			{
				Position += speed * glm::normalize(glm::cross(Orientation, up));
			}
			if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
			{
				Position += speed * up;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
			{
				Position += speed * -up;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			{
				speed = 0.4f;
			}
			else if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
			{
				speed = 0.1f;
			}
			if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS )
			{
				lineLoop = !lineLoop;	
			}
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
			{
				glfwSetWindowShouldClose(window, true);
			}

}
