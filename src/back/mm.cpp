#include "window.hpp"
#include "shader.hpp"
#include "buffers.hpp"
#include "mesh.hpp"

void error( int error, const char* d ) { std::cout << "error: " << d << std::endl; }
void sizeCallback( GLFWwindow* window, int width, int height ) { glViewport( 0, 0, width, height ); }

int main(  )
{

	Window window( 640, 480, "Bread" );
	GLFWwindow* win = window.init();

	glfwSetErrorCallback          ( error          );
	//glfwSetKeyCallback            ( win, keys         );
	glfwMakeContextCurrent        ( win               );
	glfwSetFramebufferSizeCallback( win, sizeCallback );
	glewInit();

	GLfloat vertices[] =
	{
		-0.5f, -0.5f     * float(sqrt(3)) / 3, 0.0f, // Lower left corner
		 0.5f, -0.5f     * float(sqrt(3)) / 3, 0.0f, // Lower right corner
		 0.0f, 0.5f      * float(sqrt(3)) * 2  / 3, 0.0f, // Upper corner
		-0.5f / 2, 0.5f  * float(sqrt(3)) / 6, 0.0f, // Inner left
		 0.5f / 2, 0.5f  * float(sqrt(3)) / 6, 0.0f, // Inner right
		 0.0f, -0.5f     * float(sqrt(3)) / 3, 0.0f // Inner down
	};

	GLfloat indices[] =
	{
		0, 3, 5,
		3, 2, 4,
		5, 4, 1
	};

	Shader shader( "v.glsl", "f.glsl" );
	Mesh mesh( vertices, indices, shader );
	Buffers buffer;

	buffer.BindVAO();
	buffer.vboData();
	buffer.eboData();
	buffer.Unbind();


	while( !glfwWindowShouldClose( win ) )
	{
		glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT );
		
		buffer.BindVAO();
		buffer.draw( sizeof(indices) );

		shader.compile();
		glfwSwapBuffers( win );
		glfwSwapInterval( 1 );
		glfwPollEvents();
	}
}
