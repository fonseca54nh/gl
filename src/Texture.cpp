#include "Texture.hpp"

Texture::Texture()
{

}

Texture::Texture( unsigned int &texture, std::string texturePath, unsigned int vao, unsigned int texVbo, unsigned int shader )
{
	glGenTextures( 1, &texture );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texture );

	SDL_Surface* sur = IMG_Load( texturePath.c_str() );
	if( !sur ) std::cout << "Failed loading texture file!" << std::endl;
	SDL_Surface* s2 = SDL_ConvertSurfaceFormat(sur, SDL_PIXELFORMAT_RGBA32, 0);

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, s2->w, s2->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, s2->pixels);
	glGenerateMipmap( GL_TEXTURE_2D );

	glUniform1i( glGetUniformLocation( shader, "tex0" ), 0 );
}

void Texture::create( unsigned int &texture, std::string texturePath, unsigned int vao, unsigned int texVbo, unsigned int shader )
{
	glGenTextures( 1, &texture );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texture );

	SDL_Surface* sur = IMG_Load( texturePath.c_str() );
	if( !sur ) std::cout << "Failed loading texture file!" << std::endl;
	SDL_Surface* s2 = SDL_ConvertSurfaceFormat(sur, SDL_PIXELFORMAT_RGBA32, 0);

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, s2->w, s2->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, s2->pixels);
	glGenerateMipmap( GL_TEXTURE_2D );

	glUniform1i( glGetUniformLocation( shader, "tex0" ), 0 );
}
