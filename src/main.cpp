#include "Load.hpp"
//#include "Shader.hpp"
#include "Camera.hpp"
#include "Gui.hpp"

auto createWindow() -> GLFWwindow*;
auto genShaders() 	-> sss;
auto genTerrain( GLuint shader )   -> void;

int main()
{
	auto window = createWindow();

	auto [vertexShader, fragmentShader, shaderProgram] = genShaders();

	//Shader base( "shaders/v.glsl", "shaders/f.glsl" );
	//auto shaderProgram = base.getProgram();

	Camera camera( window, shaderProgram );

	Load tree( "objs/table.obj", "assets/wood.jpg", shaderProgram );
	Load cube( "objs/b.obj", "assets/brick.jpg", shaderProgram );
	Load tile( "objs/tile.obj", "assets/texture.png", shaderProgram );
	Load landTile( "objs/tile.obj", "assets/texture1.png", shaderProgram );
	Load wtile( "objs/tile.obj", "assets/blue.png", shaderProgram );
	Load gui( "objs/floor.obj", "assets/grass.jpg", shaderProgram );
	Load floor( "objs/floor.obj", "assets/4k.jpg", shaderProgram );

	Gui topbar;
	Gui button;
	
	auto lastTime = glfwGetTime();
	auto frames = 0;
	std::string f = "FPS: ", fps, lastFps = " ";

	unsigned int shadowMap;
	glGenFramebuffers( 1, &shadowMap );
	
	unsigned int sw = 1024, sh = 1024;
	unsigned int smap;
	glGenTextures( 1, &smap );
	glBindTexture( GL_TEXTURE_2D, smap );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, sw, sh, 0,GL_DEPTH_COMPONENT, GL_FLOAT, NULL );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
	float cc[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, cc );

	glBindFramebuffer( GL_FRAMEBUFFER, shadowMap );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, smap, 0 );
	glDrawBuffer( GL_NONE );
	glReadBuffer( GL_NONE );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );

	glm::mat4 ort = glm::ortho( -35.0f, 35.0f, -35.0f, 35.0f, 0.1f, 75.0f );
	glm::vec3 lightPos( 1.0f, 2.0f, 1.0f );
	glm::mat4 lightview = glm::lookAt( 20.0f * lightPos, glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3(0.0f, 0.0f, 0.0f) );
	glm::mat4 lProj = ort * lightview;

	while( !glfwWindowShouldClose( window ) )
	{
			glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
			glClearColor( 0.9f, 0.9f, 0.9f, 1.0f );
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

			auto newShader = dynaRec( vertexShader, fragmentShader, shaderProgram );

			glUniformMatrix4fv( glGetUniformLocation( newShader, "lmat" ), 1, GL_FALSE, glm::value_ptr( lProj ) );
			glUseProgram( newShader );

			camera.input();
			int n = 10;
			float apperture = 0.01;
			glm::vec3 right = glm::normalize( glm::cross( Position - Orientation, up ) );
			glm::vec3 pUp = glm::normalize( glm::cross( Position - Orientation, right ) );
			for( auto i = 0;  i< n; i++ )
			{
				glm::vec3 bokeh = right * cosf( i*2*3.14 / n ) + pUp * sinf( i*2*3.14 / n );
				glm::mat4 model = glm::lookAt( Position + apperture * bokeh, Orientation, pUp );
				glm::mat4 camMatrix = projection * model;
				glUniformMatrix4fv( glGetUniformLocation( newShader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( camMatrix ) );
				glAccum( i ? GL_ACCUM : GL_LOAD, 1.0 / n );
			}
			glAccum( GL_RETURN, 1 );

			glEnable( GL_DEPTH_TEST );
			glViewport( 0, 0, sw, sh );
			glBindFramebuffer( GL_FRAMEBUFFER, shadowMap );
			glClear( GL_DEPTH_BUFFER_BIT );

			tree.useShader( newShader );
			tree.draw( 2.0f, -1.f, 0.0f, 1 );

			glBindFramebuffer( GL_FRAMEBUFFER, 0 );

			glUniformMatrix4fv( glGetUniformLocation( newShader, "lmat" ), 1, GL_FALSE, glm::value_ptr( glm::mat4(1.0f) ) );
			glViewport( 0, 0, width, height );
			tree.draw( 2.0f, -1.f, 0.0f, 1 );

			floor.useShader( newShader );
			landTile.useShader( newShader );
			tile.useShader( newShader );
			wtile.useShader( newShader );

			glUseProgram( newShader );
			floor.draw( 0.0f, -2.0f, 0.0f, 20 );

			auto bshader = dynaRecBUTTON( vertexShader, fragmentShader, shaderProgram );
			glUseProgram( bshader );
			gui.useShader( bshader );

			float mouse = 0.0f;
			double xpos, ypos;

			glfwGetCursorPos( window, &xpos, &ypos );
			glm::vec2 pos = glm::vec2(xpos/width, ypos/height);

			//std::cout << "xpos: " << pos.x << std::endl;
			//std::cout << "ypos: " << pos.y << std::endl;

			auto c = 0;
			
			if( ( 0.45f ) < pos.x and pos.x < (0.55f) and ( 0.45f ) < pos.y and pos.y < ( 0.55f ) )
			{
				mouse = 1.0f;
				glUniform1f( glGetUniformLocation( bshader, "mouse" ), mouse );
			}
			else
			{
				mouse = 0.0f;
				glUniform1f( glGetUniformLocation( bshader, "mouse" ), mouse );
				glUseProgram( bshader );
			}

			int click = 0;
			if ( glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS )
			{
				if( ( 0.45f ) < pos.x and pos.x < (0.55f) and ( 0.45f ) < pos.y and pos.y < ( 0.55f ) )
					//click = 1;
					tree.bind( "assets/mbrick.jpg" );
				else 
					click = 0;
			}

			gui.draw( 0.5f, 0.0f, 0.0f, 1 );

			auto ui = dynaRecGUI( vertexShader, fragmentShader, shaderProgram );
			topbar.useShader( ui );
			glUseProgram( ui );
			topbar.draw();

			glm::vec3 textPosition = glm::vec3(-0.99f, 0.99f, 0.0f);
			auto fshader = dynaRecTEXT( vertexShader, fragmentShader, shaderProgram );
			glUseProgram( fshader );
			glUniform3fv( glGetUniformLocation( fshader, "position" ), 1, glm::value_ptr(textPosition));
			glUniform1i( glGetUniformLocation( fshader, "scale" ), 4 );

			auto currentTime = glfwGetTime();
			frames++;
			
			if( currentTime - lastTime > 1.0 )
			{
				fps = std::to_string( frames/int( currentTime - lastTime ) );
				frames = 0;
				lastTime += 1.0;
				a( fshader, f + fps );
				lastFps = f + fps;
			}
			else
			{
				a( fshader, lastFps );
			}

			textPosition  = glm::vec3( -0.1f, 0.035f, 0.0f );
			auto bfshader = dynaRecTEXT( vertexShader, fragmentShader, shaderProgram );
			glUseProgram( bfshader );
			glUniform3fv( glGetUniformLocation( bfshader, "position" ), 1, glm::value_ptr( textPosition ) );
			glUniform1i( glGetUniformLocation( bfshader, "scale" ), 5 );
			glUniform1f( glGetUniformLocation( bfshader, "mouse" ), mouse );
			glUniform1i( glGetUniformLocation( bshader, "click" ), click );
			a( bfshader, "Click Me!" );

			glBindFramebuffer( GL_FRAMEBUFFER, 0 );

			glfwSwapBuffers( window );
			glfwSwapInterval( 1 );
			glfwPollEvents();

	}
}

auto createWindow() -> GLFWwindow*
{
	if( !glfwInit() ) { std::cout << "init error" << std::endl; }
	glfwWindowHint( GLFW_SAMPLES, 16 );
	glEnable( GL_MULTISAMPLE );
	GLFWwindow* window = glfwCreateWindow( width, height, "Bread", NULL, NULL );
	if( !window ) std::cout << "window error" << std::endl;
	glfwMakeContextCurrent( window ) ;
	glewInit();

	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_BLEND );
	glEnable( GL_TEXTURE_2D );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	//glDepthFunc( GL_LESS );

	IMG_Init( IMG_INIT_JPG | IMG_INIT_PNG );

	GLfloat white[]     = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat darkGrey[]  = { 0.1f, 0.1f, 0.1f, 1.0f };
	GLfloat lightGray[] = { 0.75f, 0.75f, 0.75f, 1.0f };

	glLightfv( GL_LIGHT0, GL_AMBIENT, darkGrey );
	glLightfv( GL_LIGHT0, GL_DIFFUSE, darkGrey );
	glLightfv( GL_LIGHT0, GL_SPECULAR, darkGrey );

	glShadeModel( GL_SMOOTH );

	return window;
}

auto genShaders() -> sss
{
	sss temp;

	temp.a = glCreateShader( GL_VERTEX_SHADER );
	temp.b = glCreateShader( GL_FRAGMENT_SHADER );
	temp.c = glCreateProgram();
	temp.c = dynaRec( temp.a, temp.b, temp.c );
	glUseProgram( temp.c );

	return temp;
}

auto getShader( std::string shader ) -> std::string
{
	std::ifstream frag( shader, std::fstream::in );
	std::string sourceCode;

	if( frag.is_open() ) sourceCode = std::string( std::istreambuf_iterator<char>(frag),
																								 std::istreambuf_iterator<char>() );
	return sourceCode;
};

void compileShader( unsigned int shader, std::string type )
{
	glCompileShader( shader );
	int compilationSuccess;
	char infoLog[512];
	glGetShaderiv( shader, GL_COMPILE_STATUS, &compilationSuccess );

	if( !compilationSuccess )
	{
		glGetShaderInfoLog( shader, 512, NULL, infoLog );
		std::cout << type << "ShaderCompilationError: " << infoLog << std::endl;
	}
	else
	{
		//std::cout << type << "ShaderCompilationSucessfull" << std::endl;
	}
}

unsigned int dynaRec( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( "shaders/v.glsl" );
	auto ftemp = getShader( "shaders/f.glsl" );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

unsigned int dynaRecGUI( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( "shaders/ui.glsl" );
	auto ftemp = getShader( "shaders/fui.glsl" );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

unsigned int dynaRecTEXT( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( "shaders/tv.glsl" );
	auto ftemp = getShader( "shaders/tf.glsl" );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

unsigned int dynaRecBUTTON( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( "shaders/vbutton.glsl" );
	auto ftemp = getShader( "shaders/fbutton.glsl" );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

unsigned int dynaRecTERRAIN( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( "shaders/vterrain.glsl" );
	auto ftemp = getShader( "shaders/fterrain.glsl" );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

void a( GLint shader, std::string message )
{
	glEnable( GL_BLEND );
	glDisable( GL_LIGHTING );

	int vPort[4];
	glGetIntegerv( GL_VIEWPORT, vPort );
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();

	//glOrtho(0, vPort[2], 0, vPort[3], -1, 1);
	glOrtho(0, vPort[2], 0, vPort[3], -0, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glBlendFunc(GL_ONE, GL_ONE);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  

	TTF_Init();
	int ptsize;
	TTF_Font* font = TTF_OpenFont( "assets/fira.ttf", 21 );

	//if( !font ) std::cout << "Error opening font! " << TTF_GetError()      << std::endl;
	//else				std::cout << "Loaded font successfully!" << std::endl;

	SDL_Color color = {255, 255, 255, 0};
	//char message[] = "Hello There!";

	SDL_Surface* fontSurface = TTF_RenderText_Blended( font, message.c_str(), color );

	//!fontSurface ? std::cout << "Could not create a font surface" << std::endl : std::cout << "Font surface created successfully!" << std::endl;

	SDL_Surface* ff;
	ff = SDL_CreateRGBSurface(0, fontSurface->w, fontSurface->h, 32, 
		0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
	SDL_BlitSurface( fontSurface, 0, ff, 0 );

	unsigned texture = 0;
	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );

	glTexImage2D(GL_TEXTURE_2D, 0, 4, fontSurface->w, fontSurface->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, ff->pixels );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap( GL_TEXTURE_2D );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);   
	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, texture );

	glColor3f(1.0f, 1.0f, 1.0f);
	unsigned int vao, vbo, texVbo;
	glGenVertexArrays( 1, &vao );
	glGenBuffers( 1, &vbo );

	glBindVertexArray( vao );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	
	GLfloat v[] = {
		10                           , 200                           , 0,
		10 + float( fontSurface->w ) , 200                           , 0,
		10 + float( fontSurface->w ) , 200 + float( fontSurface->h ) , 0,
		10                           , 200 + float( fontSurface->h ) , 0
	};

	float xpos = 0.1f, ypos = 0.1f;
	//std::cout << fontSurface->h << " vs " << float( fontSurface->h ) << std::endl;
	float h = float( fontSurface->h ),
				w = float( fontSurface->w );
	//float vertices[6][4] = {
	//						xpos,     ypos + h,   0.0f, 0.0f ,            
	//						xpos,     ypos,       0.0f, 1.0f ,
	//						xpos + w, ypos,       1.0f, 1.0f ,

	//						xpos,     ypos + h,   0.0f, 0.0f ,
	//						xpos + w, ypos,       1.0f, 1.0f ,
	//						xpos + w, ypos + h,   1.0f, 0.0f            
	//				};

	GLfloat vertices[] = {
	0.0f, 0.0f , 0,
	0.0f, 1.0f , 0,
	1.0f, 1.0f , 0,

	0.0f, 0.0f , 0,
	1.0f, 1.0f , 0,
	1.0f, 0.0f , 0
					};

	//float ts[] = {
	//	0.0f, 0.0f, 
	//	0.0f, 1.0f, 
	//	1.0f, 1.0f, 
	//	0.0f, 0.0f, 
	//	1.0f, 1.0f, 
	//	1.0f, 0.0f, 
	//};

	float ts[] = {
		0.0f, 0.0f, 
		0.0f, 1.0f, 
		1.0f, 1.0f, 
		0.0f, 0.0f, 
		1.0f, 1.0f, 
		1.0f, 0.0f, 
	};

	GLfloat t[] = {
		0, 1,
		1, 1,
		1, 0,
		0, 0,
	};
	
	glBufferData( GL_ARRAY_BUFFER, sizeof( vertices )*24, vertices, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, texVbo );
	glBufferData( GL_ARRAY_BUFFER, sizeof(ts), ts, GL_STATIC_DRAW );
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray(1);  

	glUniform1i( glGetUniformLocation( shader, "tex0" ), 0 );

	glBindTexture( GL_TEXTURE_2D, texture );
	glDrawArrays(GL_TRIANGLES, 0, sizeof( vertices ));

	//Disable blending
	glDisable(GL_BLEND);
	
	//Clean up
	SDL_FreeSurface(fontSurface);
	SDL_FreeSurface(ff);
	glDeleteTextures(1, &texture);

	TTF_CloseFont( font );

	glEnable( GL_LIGHTING );
}

void aaa()
{
	glEnable( GL_BLEND );
	glDisable( GL_LIGHTING );

	int vPort[4];
	glGetIntegerv( GL_VIEWPORT, vPort );
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();

	glOrtho(0, vPort[2], 0, vPort[3], -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glBlendFunc(GL_ONE, GL_ONE);

	TTF_Init();
	int ptsize;
	TTF_Font* font = TTF_OpenFont( "assets/font.ttf", 22 );

	if( !font ) std::cout << "Error opening font! " << TTF_GetError()      << std::endl;
	else				std::cout << "Loaded font successfully!" << std::endl;

	SDL_Color color = {255, 255, 1, 0};
	char message[] = "Hello There!";

	SDL_Surface* fontSurface = TTF_RenderText_Blended( font, message, color );

	//!fontSurface ? std::cout << "Could not create a font surface" << std::endl : std::cout << "Font surface created successfully!" << std::endl;

	SDL_Surface* ff;
	ff = SDL_CreateRGBSurface(0, fontSurface->w, fontSurface->h, 32, 
		0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
	SDL_BlitSurface( fontSurface, 0, ff, 0 );

	unsigned texture = 0;
	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );

	glTexImage2D(GL_TEXTURE_2D, 0, 4, fontSurface->w, fontSurface->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, ff->pixels );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap( GL_TEXTURE_2D );

	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, texture );

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin( GL_QUADS );
	{
		glTexCoord2f( 0, 1 ) ; glVertex2f( 200, 200                                   ) ;
		glTexCoord2f( 1, 1 ) ; glVertex2f( 200 + fontSurface->w, 200                  ) ;
		glTexCoord2f( 1, 0 ) ; glVertex2f( 200 + fontSurface->w, 200 + fontSurface->h ) ;
		glTexCoord2f( 0, 0 ) ; glVertex2f( 200, 200 + fontSurface->h                  ) ;
	}
	glEnd();
	//Disable blending
	glDisable(GL_BLEND);
	
	//Clean up
	SDL_FreeSurface(fontSurface);
	SDL_FreeSurface(ff);
	glDeleteTextures(1, &texture);

	TTF_CloseFont( font );

	glEnable( GL_LIGHTING );
}

auto genTerrain( GLuint shader ) -> void
{
	vertice temp;
	std::vector<vertice> vertices;
	for( auto z = 0.0f; z <= 20; z++ )
	{
		for( auto x = 0.0f; x <= 20; x++ )
		{
			temp.x = x, temp.y = 0.0f, temp.z = z;
			vertices.push_back(temp);
		}
	}

	std::vector<float> vv;

	for( auto i : vertices )
	{
		vv.push_back( i.x );
		vv.push_back( i.y );
		vv.push_back( i.z );
	}

	for( auto i : vv ) std::cout << i << std::endl;
	GLfloat uiVertices[] = {
			-1.f,0,-1.f, 
			+1.f,0,-1.f, 
			+1.f,0,+1.f, 
			+1.f,0,+1.f, 
			-1.f,0,+1.f, 
			-1.f,0,-1.f, 
	};

	GLfloat hexagon[] = {
		+0 , 0 , +1   , 
		+1 , 0 , -0.5 , 
		+1 , 0 , +0.5 , 
                    
		+0 , 0 , +1   , 
		+0 , 0 , -1   , 
		+1 , 0 , -0.5 , 
                    
		+0 , 0 , +1   , 
		-1 , 0 , +0.5 , 
		+0 , 0 , -1.0 , 
                    
		-1 , 0 , +0.5 , 
		-1 , 0 , -0.5 , 
		+0 , 0 , -1   , 
	};

	//for( auto i : vertices ) std::cout << i.x << " " << i.y << " " << i.z << std::endl;

	unsigned int vao, vbo;

	glGenVertexArrays( 1, &vao );
	glGenBuffers( 1, &vbo );

	glBindVertexArray( vao );

	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, sizeof(hexagon), hexagon, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	model = glm::mat4(1.0f);
	model = glm::translate( model, glm::vec3(0.0f, 0.0f, 0.0f) );
	glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );

	//glBindVertexArray( vao );
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawArrays( GL_TRIANGLE_STRIP, 0, sizeof( hexagon ) );
	//glPolygonMode(GL_FRONT_AND_BACK, GL_TRIANGLES);
}

			//landTile.draw ( +0.00f , -1.0f , +0.00f );
			//tile.draw     ( 1*(+1.75f) , -1.0f , +0.00f );
			//landTile.draw ( 2*(+1.75f) , -1.0f , +0.00f );
			//wtile.draw		( 3*(+1.75f) , -1.0f , +0.00f );

			//landTile.draw ( +0.00f +0.87f, -1.0f , +1.75f );
			//tile.draw     ( 1*(+1.75f)+0.87f , -1.0f , +1.75f );
			//landTile.draw ( 2*(+1.75f)+0.87f , -1.0f , +1.75f );
			//wtile.draw		( 3*(+1.75f)+0.87f , -1.0f , +1.75f );

			//landTile.draw ( +0.00f +0.87f    , -0.5f , -1.5f );
			//tile.draw     ( 1*(+1.75f)+0.87f , -0.5f , -1.5f );
			//landTile.draw ( 2*(+1.75f)+0.87f , -0.5f , -1.5f );
			//wtile.draw		( 3*(+1.75f)+0.87f , -0.5f , -1.5f );

			//landTile.draw ( +0.00f , -1.0f , +0.00f );
			//tile.draw     ( +1.75f , -1.0f , +0.00f );
			//landTile.draw ( +0.87f , -1.0f , -1.50f );
			//wtile.draw		( +0.87f , -1.0f , +1.50f );

			//landTile.draw ( 2*(+0.00f) , -1.0f , 2*(+0.00f ) );
			//tile.draw     ( 2*(+1.75f) , -1.0f , 2*(+0.00f ) );
			//landTile.draw ( 2*(+0.87f) , -1.0f , 2*(-1.50f ) );
			//wtile.draw		( 2*(+0.87f) , -1.0f , 2*(+1.50f ) );

			//landTile.draw ( -2*(+0.00f) , -1.0f , -2*(+0.00f ) );
			//tile.draw     ( -2*(+1.75f) , -1.0f , -2*(+0.00f ) );
			//landTile.draw ( -2*(+0.87f) , -1.0f , -2*(-1.50f ) );
			//wtile.draw		( -2*(+0.87f) , -1.0f , -2*(+1.50f ) );

			//landTile.draw ( 3*(+0.00f) , -1.0f , 3*(+0.00f ) );
			//tile.draw     ( 3*(+1.75f) , -1.0f , 3*(+0.00f ) );
			//landTile.draw ( 3*(+0.87f) , -1.0f , 3*(-1.50f ) );
			//wtile.draw		( 3*(+0.87f) , -1.0f , 3*(+1.50f ) );

			//landTile.draw ( -3*(+0.00f) , -1.0f , -3*(+0.00f ) );
			//tile.draw     ( -3*(+1.75f) , -1.0f , -3*(+0.00f ) );
			//landTile.draw ( -3*(+0.87f) , -1.0f , -3*(-1.50f ) );
			//wtile.draw		( -3*(+0.87f) , -1.0f , -3*(+1.50f ) );

			//landTile.draw ( 4*(+0.00f) , -1.0f , 4*(+0.00f ) );
			//tile.draw     ( 4*(+1.75f) , -1.0f , 4*(+0.00f ) );
			//landTile.draw ( 4*(+0.87f) , -1.0f , 4*(-1.50f ) );
			//wtile.draw		( 4*(+0.87f) , -1.0f , 4*(+1.50f ) );

			//wtile.draw( 0.0f, -1.0f, 1.75f );

				//std::cout << "====================model====================" << std::endl;
				//for ( auto i = 0; i < model.length(); ++i )
				//{
				//	for ( auto j = 0; j < model.length(); ++j )
				//	{
				//		if( c == 4 ) 
				//		{
				//			std::cout << std::endl;
				//			c = 0;

				//		}	
				//		std::cout << model[i][j] << " ";
				//		c++;
				//	}
				//}
				//std::cout << std::endl << "====================model====================" << std::endl;
