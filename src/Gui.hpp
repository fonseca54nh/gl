#pragma once

#ifndef __GUI_HPP
#define __GUI_HPP

#include "header.hpp"

class Gui
{
	protected:
		unsigned int vao, vbo;
		GLfloat *vertices;
		GLint shader;
		
	public:
		Gui();
		Gui( GLfloat *vertices );
		Gui( GLfloat *vertices, GLint );

		void draw();
		void useShader( GLint );
		void print();
};

#endif
