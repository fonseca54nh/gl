#pragma once 

#ifndef __HEADER_HPP
#define __HEADER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <typeinfo>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

//#include "stbi.h"

typedef struct vertice { GLfloat x, y, z; } vertice;
typedef struct vector2 { GLfloat x, y;    } vector2;

struct mesh
{
	std::vector<float> vertices;
	std::vector<int>   indices;
	std::vector<float> texCoords;
	std::vector<float> normals;
};

struct sss{ GLuint a, b, c; };

inline glm::vec3 Position    = glm::vec3( 0.0f , 0.0f ,  2.5f );
inline glm::vec3 Orientation = glm::vec3( 0.0f , 0.0f , -1.0f );
inline glm::vec3 up          = glm::vec3( 0.0f , 1.0f ,  0.0f );

inline float width = 640 , height      = 480;
inline float speed = 0.1f , sensitivity = 100.0f;

inline glm::mat4 model		   = glm::mat4(1.0f);

inline glm::mat4 view = glm::lookAt( Position, Position + Orientation, up );
inline glm::mat4 projection = glm::perspective( glm::radians(45.0f), (float)(width / height), 0.9f, 100.f );

//model readFile();
auto getShader( std::string shader ) -> std::string;
auto genShaders() 	-> sss;
void compileShader( unsigned int shader, std::string type );
unsigned int dynaRec( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram );
unsigned int dynaRecGUI( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram );
unsigned int dynaRecTEXT( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram );
unsigned int dynaRecTERRAIN( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram );
unsigned int dynaRecBUTTON( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram );
void a( GLint shader, std::string message );

#endif
