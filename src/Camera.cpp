#include "Camera.hpp"

void Camera::init()
{
	glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );
}

void Camera::updateView()
{
		view = glm::lookAt( Position, Position + Orientation, up );
		projection = glm::perspective( glm::radians(45.0f), (float)(width / height), 0.1f, 100.f );

		glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );
}

void Camera::input()
{
			if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			{
				Position += speed * Orientation;
			}
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			{
				Position += speed * -glm::normalize(glm::cross(Orientation, up));
			}
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			{
				Position += speed * -Orientation;
			}
			if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			{
				Position += speed * glm::normalize(glm::cross(Orientation, up));
			}
			if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
			{
				Position += speed * up;
			}
			if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
			{
				Position -= speed * up;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
			{
				Orientation = glm::rotate( Orientation, -glm::radians(2.f), up);
			}
			if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
			{
				Orientation = glm::rotate( Orientation, glm::radians(2.f), up);
			}
			if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
			{
				float rotx = 1.0f * (float)( 0.8f - (height / 2l) ) / height;

				glm::vec3 newO = glm::rotate( Orientation, glm::radians(rotx), glm::normalize( glm::cross(Orientation, up) ) );
				if( !(glm::angle(newO, up) <= glm::radians(5.0f)
						or glm::angle(newO,-up) <= glm::radians(5.0f)))
				{Orientation = newO;}
			}
			if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
			{
				float rotx = 1.0f * (float)( 0.8f - (height / 2l) ) / height;
				//float roty = 0.1f * (float)( 0.2f - (height / 2l) ) / height;
				glm::vec3 newO = glm::rotate( Orientation, glm::radians(-rotx), glm::normalize( glm::cross(Orientation, up) ) );
				if( !(glm::angle(newO, up) <= glm::radians(5.0f)
						or glm::angle(newO,-up) <= glm::radians(5.0f)))
				{Orientation = newO;}

			}
			if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
			{
				Position += speed * -up;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			{
				speed = 0.4f;
			}
			else if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
			{
				speed = 0.1f;
			}
			//if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS )
			//{
			//	lineLoop = !lineLoop;	
			//}
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
			{
				glfwSetWindowShouldClose(window, true);
			}
			if ( glfwGetKey( window, GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS )
			{
				glfwGetCursorPos( window, &xpos, &ypos );
				std::cout << "xpos: " << xpos << std::endl;
				std::cout << "ypos: " << ypos << std::endl;
			}

			updateView();
			
			//glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );
}

void Camera::updateShader( GLuint ns ) { shader = ns; }
