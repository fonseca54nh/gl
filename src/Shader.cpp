#include "Shader.hpp"

Shader::Shader( std::string v, std::string f ) 
	: vertexPath( v ), fragmentPath( f )
{
	vertexShader   = glCreateShader( GL_VERTEX_SHADER );
	fragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
	shaderProgram  = glCreateProgram();
	shaderProgram  = dynaRec( vertexShader, fragmentShader, shaderProgram );
	glUseProgram( shaderProgram );
}

auto Shader::getShader( std::string shader ) -> std::string
{
	std::ifstream frag( shader, std::fstream::in );
	std::string sourceCode;

	if( frag.is_open() ) sourceCode = std::string( std::istreambuf_iterator<char>(frag),
																								 std::istreambuf_iterator<char>() );
	return sourceCode;
};

void Shader::compileShader( unsigned int shader, std::string type )
{
	glCompileShader( shader );

	int compilationSuccess;
	char infoLog[512];
	glGetShaderiv( shader, GL_COMPILE_STATUS, &compilationSuccess );

#ifdef SHADER_DEBUG
	if( !compilationSuccess )
	{
		glGetShaderInfoLog( shader, 512, NULL, infoLog );
		std::cout << type << "ShaderCompilationError: " << infoLog << std::endl;
	}
	else
	{
		std::cout << type << "ShaderCompilationSucessfull" << std::endl;
	}
#endif
}

unsigned int Shader::dynaRec( unsigned int vertexShader, unsigned int fragmentShader, unsigned int shaderProgram )
{
	auto vtemp = getShader( vertexPath 	 );
	auto ftemp = getShader( fragmentPath );
	auto *vertexShaderSource  = vtemp.c_str();
	auto fragmentShaderSource = ftemp.c_str();

	glShaderSource( vertexShader, 1, &vertexShaderSource, NULL);
	compileShader( vertexShader, "vertex");

	glShaderSource( fragmentShader, 1, &fragmentShaderSource, NULL);
	compileShader( fragmentShader, "frag" );

	glDeleteProgram( shaderProgram );
	shaderProgram = glCreateProgram();

	// attach the compiled shaders to a shader program
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glLinkProgram ( shaderProgram );

	return shaderProgram;
}

void Shader::useShader()
{
	glUseProgram( shaderProgram );
}

GLint Shader::getProgram()
{
	return shaderProgram;
}
