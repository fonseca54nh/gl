#pragma once

#ifndef __SHADER__HPP
#define __SHADER__HPP

#include "header.hpp"

class Shader
{
	protected:
		GLuint vertexShader, 
					 fragmentShader,
					 shaderProgram;

		std::string vertexPath, fragmentPath;
		
	public:
		Shader( std::string v, std::string f );
								
		auto getShader( std::string shader ) -> std::string;
		void compileShader( unsigned int shader, std::string type );
		unsigned int dynaRec( unsigned int vertexShader, 
													unsigned int fragmentShader,
													unsigned int shaderProgram );
		void useShader();
		GLint getProgram();
};

#endif
