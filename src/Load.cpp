#include "Load.hpp"

Load::Load( std::string a, std::string b, GLuint s )
	: filePath(a), texturePath( b ), shader( s )
{
	auto [vertices, indices, texCoords, normals] = readfile();

	glGenVertexArrays( 1, &vao );
	glGenBuffers( 1, &vbo );
	glGenBuffers( 1, &texVbo );

	glBindVertexArray( vao );

	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, vertices.size()*12, vertices.data(), GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, texVbo );
	glBufferData( GL_ARRAY_BUFFER, texCoords.size()*12, texCoords.data(), GL_STATIC_DRAW );
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
	glEnableVertexAttribArray(1);  

	//create( texture, texturePath, texCoords, vao, texVbo, shader );
	//Texture tex;
	//tex.create( texture, texturePath, vao, texVbo, shader );
	Texture( texture, texturePath, vao, texVbo, shader );

	unsigned int nvbo;

	glGenBuffers( 1, &nvbo );
	glBindBuffer( GL_ARRAY_BUFFER, nvbo );
	glBufferData( GL_ARRAY_BUFFER, normals.size(), normals.data(), GL_STATIC_DRAW );

	glEnableVertexAttribArray( 2 );
	glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );

	size = vertices.size();

	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	GLuint depthTexture;
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT, 1024, 1024, 0,GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	glDrawBuffer(GL_NONE); // No color buffer is drawn to.
	glReadBuffer(GL_NONE); // No color buffer is drawn to.

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);

	glBindFramebuffer( GL_FRAMEBUFFER, 0 );

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	 std::cout << "Olha aqui" << std::endl;

	glm::vec3 lightInvDir = glm::vec3(0.5f,2,2);

	// Compute the MVP matrix from the light's point of view
	glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10,10,-10,10,-10,20);
	glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0,0,0), glm::vec3(0,1,0));
	glm::mat4 depthModelMatrix = glm::mat4(1.0);
	glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(glGetUniformLocation( shader, "lmat" ), 1, GL_FALSE, &depthMVP[0][0]);
}

//Load::Load( std::string a, std::string b, GLuint s )
//	: filePath(a), texturePath( b ), shader( s )
//{
//	auto [vertices, indices, texCoords, normals] = readfile();
//
//	glGenVertexArrays( 1, &vao );
//	glGenBuffers( 1, &vbo );
//	glGenBuffers( 1, &texVbo );
//
//	glBindVertexArray( vao );
//
//	glBindBuffer( GL_ARRAY_BUFFER, vbo );
//	glBufferData( GL_ARRAY_BUFFER, vertices.size()*12, vertices.data(), GL_STATIC_DRAW );
//
//	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
//	glEnableVertexAttribArray( 0 );
//
//	glBindBuffer( GL_ARRAY_BUFFER, 0 );
//
//	glGenTextures( 1, &texture );
//	glActiveTexture( GL_TEXTURE0 );
//	glBindTexture( GL_TEXTURE_2D, texture );
//
//	glBindBuffer( GL_ARRAY_BUFFER, texVbo );
//	glBufferData( GL_ARRAY_BUFFER, texCoords.size()*12, texCoords.data(), GL_STATIC_DRAW );
//	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, ( void* )0 );
//	glEnableVertexAttribArray(1);  
//
//	SDL_Surface* sur = IMG_Load( texturePath.c_str() );
//	if( !sur ) std::cout << "Failed loading texture file!" << std::endl;
//	SDL_Surface* s2 = SDL_ConvertSurfaceFormat(sur, SDL_PIXELFORMAT_RGBA32, 0);
//
//	glGenTextures( 1, &texture );
//	glActiveTexture( GL_TEXTURE0 );
//	glBindTexture( GL_TEXTURE_2D, texture );
//
//
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
//	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, s2->w, s2->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, s2->pixels);
//	glGenerateMipmap( GL_TEXTURE_2D );
//
//	// Unbind the VBO and VAO
//	glBindTexture( GL_TEXTURE_2D, 0 );
//	glBindVertexArray( 0 );
//
//	glBindVertexArray( vao );
//	// get the texture to the shader
//	glUniform1i( glGetUniformLocation( shader, "tex0" ), 0 );
//
//	unsigned int nvbo;
//
//	glGenBuffers( 1, &nvbo );
//	glBindBuffer( GL_ARRAY_BUFFER, nvbo );
//	glBufferData( GL_ARRAY_BUFFER, normals.size(), normals.data(), GL_STATIC_DRAW );
//
//	glEnableVertexAttribArray( 2 );
//	glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0 );
//
//	glBindBuffer( GL_ARRAY_BUFFER, 0 );
//	glBindVertexArray( 0 );
//
//	size = vertices.size();
//
//	//glEnable( GL_DEPTH_TEST );
//	//glGenFramebuffers( 1, &depthVbo );
//	//glBindFramebuffer( GL_FRAMEBUFFER, depthVbo );
//
//	//glGenTextures( 1, &depthMap );
//	//glBindTexture( GL_TEXTURE_2D, depthMap );
//	//glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, sw, sh, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0 );
//	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
//	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
//	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
//	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
//
//	//glFramebufferTexture( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthMap, 0 );
//	//glDrawBuffer( GL_NONE );
//	//glReadBuffer( GL_NONE );
//
//	//glm::vec3 lightInvDir = glm::vec3(0.5f,2,2);
//	//// Compute the MVP matrix from the light's point of view
//	//glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10,10,-10,10,-10,20);
//	//glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0,0,0), glm::vec3(0,1,0));
//	//glm::mat4 depthModelMatrix = glm::mat4(1.0);
//	//glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
//
//	//// Send our transformation to the currently bound shader,
//	//// in the "MVP" uniform
//	//glUniformMatrix4fv(glGetUniformLocation( shader, "lmat" ), 1, GL_FALSE, &depthMVP[0][0]);
//
// glGenFramebuffers(1, &FramebufferName);
// glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
//
// GLuint depthTexture;
// glGenTextures(1, &depthTexture);
// glBindTexture(GL_TEXTURE_2D, depthTexture);
// glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT, 1024, 1024, 0,GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//
// glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);
//
// glDrawBuffer(GL_NONE); // No color buffer is drawn to.
// glReadBuffer(GL_NONE); // No color buffer is drawn to.
//
// glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
//
// glBindFramebuffer( GL_FRAMEBUFFER, 0 );
//
// // Always check that our framebuffer is ok
// if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
//	 std::cout << "Olha aqui" << std::endl;
//
// glm::vec3 lightInvDir = glm::vec3(0.5f,2,2);
//
// // Compute the MVP matrix from the light's point of view
// glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10,10,-10,10,-10,20);
// glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0,0,0), glm::vec3(0,1,0));
// glm::mat4 depthModelMatrix = glm::mat4(1.0);
// glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
//
// // Send our transformation to the currently bound shader,
// // in the "MVP" uniform
//	glUniformMatrix4fv(glGetUniformLocation( shader, "lmat" ), 1, GL_FALSE, &depthMVP[0][0]);
//}

mesh Load::readfile()
{
	std::ifstream file( filePath, std::fstream::in );

	if( !file.is_open() ) std::cout << "Could not open Object file!" << std::endl;

	std::vector<vertice> vertices;
	std::vector<vertice> normals;
	std::vector<vector2> texCoords;

	std::vector<int> indices;
	std::vector<int> texIndices;
	std::vector<int> normalIndices;

	std::string line, id;

	int a, b, c, at, bt, ct, an, bn, cn;
	//float n;
	vertice temp;
	vector2 ttemp;
	GLfloat x, y, z;
	char f;

	while( getline( file, line ) )
	{
		std::istringstream iss( line );

		//std::cout << "line: " << line << std::endl;
		while( iss )
		{
			iss >> id;

			if( id == "v" )
			{
				iss >> x;
				temp.x = x;
				//std::cout << "x: " << temp.x << " ";

				iss >> y;
				temp.y = y;
				//std::cout << "y: " << temp.y << " ";

				iss >> z;
				temp.z = z;
				//std::cout << "z: " << temp.z << " ";
				if( iss ) vertices.push_back( temp );

				//std::cout << std::endl;
			}

			if( id == "vt" )
			{
				iss >> x;
				ttemp.x = x;
				//std::cout << "x: " << ttemp.x << " ";

				iss >> y;
				ttemp.y = y;
				//std::cout << "y: " << ttemp.y << " ";

				if( iss ) texCoords.push_back( ttemp );

				//std::cout << std::endl;
			}

			if( id == "vn" )
			{
				iss >> x;
				temp.x = x;
				//std::cout << "x: " << temp.x << " ";

				iss >> y;
				temp.y = y;
				//std::cout << "y: " << temp.y << " ";

				iss >> z;
				temp.z = z;
				//std::cout << "z: " << temp.z << " ";
				if( iss ) normals.push_back( temp );

				//std::cout << std::endl;
			}

			if( id == "f" )
			{
				iss >> a >> f >> at >> f >> an;
				if( iss ) indices.push_back( a );
				if( iss ) texIndices.push_back( at );
				if( iss ) normalIndices.push_back( an );

				iss >> b >> f >> bt >> f >> bn;
				if( iss ) indices.push_back( b );
				if( iss ) texIndices.push_back( bt );
				if( iss ) normalIndices.push_back( bn );

				iss >> c >> f >> ct >> f >> cn;
				if( iss ) indices.push_back( c );
				if( iss ) texIndices.push_back( ct );
				if( iss ) normalIndices.push_back( cn );

				//iss >> d >> f >> n >> f >> n;
				//if( iss ) indices.push_back( d );
			}
		}
	}
	mesh m;

	m.indices  = indices;

	//std::cout << "indices" << std::endl;
	//for( auto x : indices ) std::cout << x << std::endl;

	//std::cout << "m.vertices" << std::endl;
	for( auto i = 0; i < indices.size(); ++i )
	{
		m.vertices.push_back( vertices[ indices[i] - 1 ].x );
		m.vertices.push_back( vertices[ indices[i] - 1 ].y );
		m.vertices.push_back( vertices[ indices[i] - 1 ].z );
	}

	//std::cout << "texSize: " << texCoords.size() << std::endl;
	for( auto i = 0; i < texIndices.size(); ++i )
	{
		m.texCoords.push_back( texCoords[ texIndices[i] - 1 ].x );
		m.texCoords.push_back( texCoords[ texIndices[i] - 1 ].y );

		//m.texCoords.push_back( texCoords[i].x );
		//m.texCoords.push_back( texCoords[i].y );
	
	}

	for( auto i = 0; i < normalIndices.size(); ++i )
	{
		m.normals.push_back( normals[ normalIndices[i] - 1 ].x );
		m.normals.push_back( normals[ normalIndices[i] - 1 ].y );
		m.normals.push_back( normals[ normalIndices[i] - 1 ].z );

		//std::cout << "indice: " << indices[i] << std::endl;
		//std::cout << vertices[indices[i] - 1 ].x << " "
		//					<< vertices[indices[i] - 1 ].y << " "
		//					<< vertices[indices[i] - 1 ].z << "\n";
	}

	//std::cout << "m.vertices" << std::endl;
	for( auto i = 0; i < m.vertices.size(); ++i )
	{
		//std::cout << m.vertices[i] << std::endl;
		//std::cout << m.vertices[i].x << " " 
		//					<< m.vertices[i].y << " "
		//					<< m.vertices[i].z << std::endl;
	}

	return m;	
};

void Load::draw()
{
		//glViewport(0,0,640,480);
		glViewport(0,0,width,height);

		model = glm::mat4(1.0f);
		model = glm::translate( model, glm::vec3(0.0f, 0.0f, 0.0f) );
		glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );

		glBindVertexArray(vao);
		glBindTexture( GL_TEXTURE_2D, texture );
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawArrays(GL_TRIANGLES, 0, 1600);
}

void Load::draw( float x, float y, float z, int scale )
{
		glEnable( GL_DEPTH_TEST );
		//glClear(GL_DEPTH_BUFFER_BIT);
		//glViewport(0,0,640,480);
		glViewport(0,0,width,height);

		glDepthFunc( GL_LESS );

		model = glm::mat4(1.0f);
		glUniformMatrix4fv( glGetUniformLocation( shader, "model" ), 1, GL_FALSE, glm::value_ptr( model ) );
		model = glm::translate( model, glm::vec3( x, y, z ) );
		glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );
	glUniform1i( glGetUniformLocation( shader, "scale" ), scale );


		glBindVertexArray(vao);
		glBindTexture( GL_TEXTURE_2D, texture );
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//glDrawArrays(GL_TRIANGLES, 0, 1600);
		glDrawArrays(GL_TRIANGLES, 0, ( size - 1 )/2);
}

void Load::draw( float x, float y, float z, int scale, float angle )
{
		//glViewport(0,0,640,480);
		glViewport(0,0,width,height);
		
			//glm::quat rotation = glm::mat4(1.0f);
			//rotation.y = glm::radians(45.f);

			//glm::mat4 rotMat = glm::toMat4( rotation );

		model = glm::mat4(1.0f);
		model = glm::translate( model, glm::vec3( x, y , z ) );
		//model*=rotMat;
		glm::rotate( model, glm::radians( angle ), glm::vec3( 0.0f, 0.0f, 1.0f ) );
		glUniformMatrix4fv( glGetUniformLocation( shader, "camMatrix" ), 1, GL_FALSE, glm::value_ptr( projection * view * model ) );

		glBindVertexArray(vao);
		glBindTexture( GL_TEXTURE_2D, texture );
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawArrays(GL_TRIANGLES, 0, 1600);
}

void Load::useShader( GLuint ns ) { shader = ns; }

glm::mat4 Load::getModel()
{
	return model;
}

void Load::bind( std::string newTexturePath )
{
	Texture imageTexture;
	imageTexture.create( texture, newTexturePath, vao, texVbo, shader );
}
