#pragma once

#ifndef __TEXTURE_HPP
#define __TEXTURE_HPP

#include "header.hpp"

class Texture
{
	public:
			Texture();
			Texture( unsigned int&, std::string, unsigned int, unsigned int, unsigned int );

			//@brief creates a new texture
			//@param texture 		-> unsigned int
			//@param texturePath -> std::string
			//@param vao 				-> unsigned int
			//@param texVbo 			-> unsigned int
			//@param shader 			-> unsigned int
			void create( unsigned int&, std::string, unsigned int, unsigned int, unsigned int );
};

#endif
