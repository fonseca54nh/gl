#version 330 core

out vec4 FragColor;

in vec2 texCoord;
in vec3 normals;
in vec3 FragPos;
in vec3 trace;
in float scalet;

uniform sampler2D tex0;

vec4 clear = vec4( 1.0f, 1.0f, 1.0f, 1.0f );
vec3 lPos  = vec3( 7.0f, 9.5f, 9.0f );

vec3 norm = normalize( normals );
vec3 lDir = normalize( lPos - FragPos );

float diff = max( dot( norm, lDir ), 0.0 );
vec4 diffuse = diff * vec4( 0.7f, 0.5f, 0.2f, 1.0f );

float as = 0.9f;
vec4 ambient = as * vec4( 1.0f, 1.0f, 0.7f, 1.0f );

float sS = 0.5f;
vec3 viewDir = normalize( trace - FragPos );
vec3 reflectDir = reflect( -lDir, norm );
float spec = pow( max( dot( viewDir, reflectDir ), 0.0 ), 32 );
vec4 specular = sS * spec * clear;

vec2 ground = vec2( 0.0f, 0.0f );

void main()
{
	vec4 orange = vec4(0.7f, 0.3f, 0.2f, 1.0f);

	vec4 mask = texture( tex0, texCoord );

	FragColor = ( ambient + diffuse + specular ) * texture( tex0, texCoord * ( scalet ) );
	//FragColor = texture( tex0, texCoord );
	//FragColor = ff / ( 1/orange );


	//FragColor = ( ambient + diffuse ) * vec4(0.7f, 0.3f, 0.2f, 1.0f);
}
