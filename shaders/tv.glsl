
#version 330 core

layout ( location = 0 ) in vec3 aPos;
layout ( location = 1 ) in vec2 aTex;

out vec2 texCoord;

uniform float mouse;
uniform vec3 position;
uniform int scale;
//uniform mat4 camMatrix;

void main() 
{ 
	  //gl_Position = vec4(aPos, 1.0);
	  //gl_Position = vec4(aPos.x/4-0.99f, -aPos.y/16+1.0, aPos.z, 1.0f);
	  gl_Position = vec4(aPos.x/(scale)+position.x, -aPos.y/(4*scale)+position.y, aPos.z, 1.0f);

		if( mouse > 0.0 )
		{
			gl_Position = vec4(aPos.x/(scale)+position.x, -aPos.y/(3.5*scale)+position.y, aPos.z, 1.0f);
		}
		if( mouse < 1.0 )
		{
			gl_Position = vec4(aPos.x/(scale)+position.x, -aPos.y/(4*scale)+position.y, aPos.z, 1.0f);
		}

		texCoord = aTex;
}
