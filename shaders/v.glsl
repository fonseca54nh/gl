#version 330 core

layout ( location = 0 ) in vec3 aPos;
layout ( location = 1 ) in vec2 aTex;
layout ( location = 2 ) in vec3 aNor;

out vec2 texCoord;
out vec3 normals;
out vec3 FragPos;
out vec3 trace;
out float scalet;

//uniform float xoffset;
uniform mat4 lmat;
uniform mat4 camMatrix;
uniform mat4 model;
uniform int scale;

void main() 
{ 
	gl_Position = (lmat) * camMatrix * vec4(aPos*scale, 1.0);
	//gl_Position = camMatrix * vec4(aPos, 1.0);
	FragPos = vec3( model * vec4( aPos/2, 1.0 ) );
	//gl_Position = vec4( aPos.x, aPos.y, aPos.z, 1.0 ); 

	texCoord = aTex;
	normals  = aNor;

	vec4 temp = camMatrix * vec4( 1.0f, 1.0f, 1.0f, 1.0f );
	trace = vec3( temp.x, temp.y, temp.z );

	scalet = scale;
}
