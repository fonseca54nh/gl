#version 330 core

out vec4 FragColor;

in vec2 texCoord;

in float a;

uniform sampler2D tex0;

vec4 clear = vec4( 1.0f, 1.0f, 1.0f, 1.0f );

void main()
{
	FragColor = vec4(0.1f, 0.532f, 0.3f, 1.0f);

	if( a < 1.0 )
	{
		FragColor = vec4(0.9f, 0.532f, 0.3f, 1.0f);
	}
	if( a > 0.0 )
	{
		FragColor = vec4(1.0f, 0.632f, 0.4f, 1.0f);
	}

	//FragColor = clear * texture( tex0, texCoord );
}
