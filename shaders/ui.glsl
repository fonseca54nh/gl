#version 330 core

layout ( location = 0 ) in vec3 aPos;

uniform mat4 camMatrix;

void main() 
{ 
	  //gl_Position = vec4(aPos, 1.0);
	  gl_Position = vec4(0.1, 1.9, 0.1f, 1.0f) + vec4(2.2f, 0.1f, 0.2f, 1.0f) * vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
