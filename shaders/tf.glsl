#version 330 core

out vec4 FragColor;

in vec2 texCoord;

uniform sampler2D tex0;

vec4 clear = vec4( 1.0f, 1.0f, 1.0f, 1.0f );

void main()
{
	//FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);

	FragColor = clear * texture( tex0, texCoord );
	//vec4 sampled = vec4(1.0, 1.0, 1.0, texture( tex0, texCoord).r);
  //FragColor = clear * sampled;
}
