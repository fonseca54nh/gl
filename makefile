OBJS = src/main.cpp src/Load.cpp src/Camera.cpp src/Gui.cpp src/Texture.cpp #src/Shader.cpp

LIBS = src/Load.cpp src/Camera.cpp src/Gui.cpp src/Texture.cpp #src/Shader.cpp

#OLD = src/back/main.cpp
OLD = src/test.cpp #src/stb.cpp

MAIN = src/main.cpp

CC = clang++

GCC = g++

COMPILER_FLAGS = -O3 -std=c++20 -march=znver1 -mtune=znver1

DEBUGER_FLAGS = -O0 -std=c++20 -g -fstandalone-debug

LINKER_FLAGS = -lGL -lglfw -lSDL2_ttf -lSDL2_image -lGLEW -lpthread -lSDL2 #-lSOIL

MODULE_FLAGS = -std=c++20 -fmodules-ts -lGL -lglfw -lSDL2_ttf -lGLEW -lpthread -lSDL2 #-lSOIL

OBJECT_NAME = build/bin/test

DEBUG_NAME = debug/testd 

back: $(OBJS)
	   $(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJECT_NAME)

debug: $(OBJS)
	   $(CC) $(OBJS) $(DEBUGER_FLAGS) $(LINKER_FLAGS) -o $(DEBUG_NAME)

modules: $(OBJS)
	   $(GCC) $(MODULE_FLAGS) $(LINKER_FLAGS) -c src/shader.cpp -o shader.o
	   $(GCC) $(OBJS) $(MODULE_FLAGS) $(LINKER_FLAGS) shader.o -o $(OBJECT_NAME)

Texture: $(OBJS)
	   $(CC) src/Texture.cpp $(COMPILER_FLAGS) -c
		 mv *.o build/src/

Load: $(OBJS)
	   $(CC) src/Load.cpp $(COMPILER_FLAGS) -c
		 mv *.o build/src/

libs: $(OBJS)
	   $(CC) $(LIBS) $(COMPILER_FLAGS) -c
		 mv *.o build/src/

main: $(OBJS)
	   $(CC) $(MAIN) build/src/*.o $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJECT_NAME)

clean:
	   #$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJECT_NAME)
		 make libs -j17
		 make main -j17

.PHONY: debug
